# Wilzuun Scripts Library
##### By ^TFW^ Wilzuun

## DynCityStdLib
This StdLib will allow for several small cities to be spawned or for one large city to be spawned, it will account for building size, and passage between them.
<details>
<summary>Roadmap</summary>
* Cities spawn at distances noted in DynPathStdLib settings.

</details>

## DynPathStdLib
A dynamic pathway system for players to follow on a mission. Randomly generates where the next point should be while players are concentrating on mission objectives.
The idea for this project came from a conversation about Warframe, and how it had a dynamic setup of where to go to do what. I thought I could at least snag the idea of dynamic pathways and give normal Starsiege objectives at each pathway marker.

## logStdLib
A log feature set, set aside for the other scripts in this collection.

## Building Size Project
A project that is recording the size of the buildings in the game.