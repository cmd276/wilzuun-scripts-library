# Wilzuun Scripts Library
##### By ^TFW^ Wilzuun

Each Game type will not have a README.md file.
All game types will their respective read me file here.

Those readme sections will be updated over the next several days.

<details>
<summary>Assassination (asn)</summary>

#### Description
Mission type that tasks the Tenno to seek and eliminate a unique enemy boss and then return to extraction.

#### Status
Planned, not yet started.

#### Roadmap
* Randomly Spawn and mark an AI as the Assassination Target.
* Randomly spawn mission entrance points, and extraction / leave mission points.
</details><details>
<summary>Defections (dfc)</summary>

#### Description
* Endless Mission type that tasks players with escorting small squads of Kavor Defectors to an extraction point while defending against the Infested.

#### Status
Planned, not yet started.
</details><details>
<summary>Defense (def)</summary>

#### Description
* Endless Mission type in which the players must defend the assigned primary objective or objectives from being destroyed by attacking waves of enemies.

#### Status
Initial configurations in testing. Game type balancing in progress.
</details><details>
<summary>Excavation (exc)</summary>

#### Description
* Endless Mission type tasks players with searching and then extracting various artifacts buried deep within a planet's surface.

#### Status
Planned, not yet started.
</details><details>
<summary>Exterminate (ext)</summary>

#### Description
* players to kill a limited number of enemies in the area and then make it to extraction

#### Status
Planned, not yet started.
</details><details>
<summary>Hijack (hij)</summary>

#### Description
* Mission type where players must take control of a large, mobile objective and safely lead it to extraction. 

#### Status
Planned, not yet started.
</details><details>
<summary>Interception (int)</summary>

#### Description
* Endless Mission type requiring players to capture and hold set locations on the map in order to intercept enemy transmissions by reaching 100% control sooner than the enemy faction does.

#### Status
Planned, not yet started.
</details><details>
<summary>Mobile Defense (mdf)</summary>

#### Description
* Mission type requiring players to carry a datamass to 2-3 computer terminals and upload it to them. Once uploaded at each terminal, players will have to defend the terminal until hacking is completed. 

#### Status
Planned, not yet started.
</details><details>
<summary>Rescue (res)</summary>

#### Description
* Mission type requiring the player to locate a hostage and escort them to extraction

#### Status
Planned, not yet started.
</details><details>
<summary>Sabotage (sab)</summary>

#### Description
* Mission type requiring players to reach an objective, and then destroy it before heading to extraction.

#### Status
Planned, not yet started.
</details><details>
<summary>Survival (sur)</summary>

#### Description
* mission type where players will have to fight an endless, steady stream of enemies to survive for as long as possible while slowly losing life support

#### Status
Planned, not yet started.
</details>

### Original ideas. 
* [x] Dark Overrun (dov)
  * Based loosely on the Overrun game type already made by some one else. In this game type, every time you kill an enemy, an increasing amount spawn.
* [x] Tag (tag)
  * Laser weapons only. No scanners. All other items allowed. One shot, if it hits, instant kill.
<details>
<summary>Dark Overrun (dov)</summary>

#### Description
* Based loosely on the Overrun game type already made by some one else. In this game type, every time you kill an enemy, an increasing amount spawn.

#### Status
Fully featured, ready to deploy game servers.
</details>