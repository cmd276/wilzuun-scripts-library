# Wilzuun Scripts Library
##### By ^TFW^ Wilzuun

Welcome to the Vehicle Depot for the Wilzuun Scripts Library!

```cs
##--------------------------- Main Classification
//
//  Turning off either of these will override all options below. Flyers are not
//    included due to them snapping to map locations, and being very hard to
//    predict, and to hit with weapons.
//
//    Examples: tanks = true; hercs = false; Terran = true;
//      Only Terran Tanks will spawn, no Terran hercs will show up.

//  $allow["Tanks"] : boolean
//    true : Enable AI tanks
$allow["Tanks"]     = true;

//  $allow["Hercs"] : boolean
//    true : Enable AI Hercs
$allow["Hercs"]     = true;

//  $allow["Drone"] : boolean
//    true : Enable AI Drones. (These typically have no weapons, so they're pretty much easy kills.
// $allow["Drone"]     = true; // This is Broken. Research required to fix.

// Flyers aren't included at all in this project.

##--------------------------- Faction Classification
//
//  These options matter if the above options are turned on.
//    Example: Tanks = true; hercs = false; Terran = true; Knight = false;
//      Only tanks will appear. Only Terran tanks will be spawned. Knight tanks
//      will not appear at all

//  $allow["Terran"]  : boolean
//    true : Will spawn in Terran vehicles.
$allow["Terran"]    = true;

//  $allow["Knight"]  : boolean
//    true : Will spawn in Knight vehicles.
$allow["Knight"]    = true;

//  $allow["Pirate"]  : boolean
//    true : Will spawn in Pirate vehicles.
$allow["Pirate"]    = true;

//  $allow["Rebel"]  : boolean
//    true : Will spawn in Rebel vehicles.
$allow["Rebel"]     = true;

//  $allow["Cybrid"]  : boolean
//    true : Will spawn in Cybrid vehicles.
$allow["Cybrid"]    = true;

//  $allow["Metagan"] : boolean
//    true : Will spawn in Metagan vehicles.
$allow["Metagan"]   = true;

//  $allow["Special"] : boolean
//    true : Will spawn in Special vehicles.
$allow["Special"]   = false;

##--------------------------- Specialty Classification
//
//  These last two categories are in respects to groups of vehicles that players
//    may find highly annoying, or undesirable for normal game play. They still 
//    require their Faction Classification to be enabled to show up.

//  $allow["Disruptors"] : boolean
//    true : Will spawn in Disruptor Tanks.
$allow["Disruptors"]= false;

//  $allow["Artillery"] : boolean
//    true : Will spawn in Artillery Tanks.
$allow["Artillery"] = false;
```



These Vehicles are default Configurations from how the Mission Editor drops the vehicles into the game.

They are named `vehId_{ID}.veh` where `{ID}` can be any viable vehicle ID. Table below for Quick reference.

Vehicles are sorted by Vehicle ID.

| Vehicle Name | Vehicle ID|
| ---- | ---- |
| Terran Apocalypse | 1 |
| Terran Minotaur | 2 |
| Terran Gorgon | 3 |
| Terran Talon | 4 |
| Terran Basilisk | 5 |
| Paladin Tank | 6 |
| Myrmidon Tank | 7 |
| Disruptor Tank | 8 |
| Knight's Apocalypse | 10 |
| Knight's Minotaur | 11 |
| Knight's Gorgon | 12 |
| Knight's Talon | 13 |
| Knight's Basilisk | 14 |
| Knight's Paladin | 15 |
| Knight's Myrmidon | 16 |
| Knight's Disruptor | 17 |
| Cybrid Seeker | 20 |
| Cybrid Goad | 21 |
| Cybrid Shepherd | 22 |
| Cybrid Adjudicator | 23 |
| Cybrid Executioner | 24 |
| Bolo Tank | 25 |
| Recluse Tank | 26 |
| Platinum Adjudicator (SP version, not selectable) | 27 |
| Platinum Executioner (SP version, not selectable) | 28 |
| Prometheus | 29 |
| Rebel Emancipator | 30 |
| Avenger Tank | 31 |
| Dreadnought Tank | 32 |
| Rebel Olympian | 33 |
| Metagen Seeker | 35 |
| Metagen Goad | 36 |
| Metagen Shepherd | 37 |
| Metagen Adjudicator | 38 |
| Metagen Executioner | 39 |
| Platinum Adjudicator 2 | 55 |
| Platinum Executioner 2 | 56 |
| Harabec's Apocalypse | 40 |
| Caanan's Basilisk | 42 |
| Harabec's Predator | 41 |
| Harabec's Super Predator | 45 |
| Pirate's Apocalypse | 50 |
| Pirate's Dreadlock | 51 |
| Pirate's Emancipator | 52 |
| Terran Empty Cargo | 60 |
| Terran Ammo Cargo | 61 |
| Terran Big Ammo Cargo | 62 |
| Terran Big Personnel Cargo | 63 |
| Terran Fuel Cargo | 64 |
| Terran Minotaur Cargo | 65 |
| Rebel Empty Cargo | 66 |
| Rebel Ammo Cargo | 67 |
| Rebel Big Cargo Transport | 68 |
| Rebel Bix Box Cargo Transport | 69 |
| Rebel Box Cargo Transport | 70 |
| Terran Utility Truck | 71 |
| Rebel Thumper | 72 |
| Terran Starefield | 73 |
| Cybrid Artillery | 90 |
| Cybrid Omnicrawler | 94 |
| Cybrid Protector | 95 |
| Cybrid Jamma | 96 |
| Nike Artillery | 133 |
| Supressor Tank | 134 |
| Rebel bike | 138 |
| Rebel Artillery | 137 |
| SUAV Bus | 150 |
