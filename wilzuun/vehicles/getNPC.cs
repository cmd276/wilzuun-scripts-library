
##--------------------------- Header
// FILE:        getNPC.cs
// AUTHORS:     ^TFW^ Wilzuun
// LAST MOD:    Dec 25, 2019
// VERSION:     1.0r

##--------------------------- Version History
// 

##--------------------------- Notes
// Updated from the old DOV script, to allow for on the fly changes to spawned NPC.

##--------------------------- Main Classification
//
//  Turning off either of these will override all options below. Flyers are not
//    included due to them snapping to map locations, and being very hard to
//    predict, and to hit with weapons.
//
//    Examples: tanks = true; hercs = false; Terran = true;
//      Only Terran Tanks will spawn, no Terran hercs will show up.

//  $allow["Tanks"] : boolean
//    true : Enable AI tanks
$allow["Tanks"]     = true;

//  $allow["Hercs"] : boolean
//    true : Enable AI Hercs
$allow["Hercs"]     = true;

//  $allow["Drone"] : boolean
//    true : Enable AI Drones. (These typically have no weapons, so they're pretty much easy kills.
// $allow["Drone"]     = true; // This is Broken. Research required to fix.

// Flyers aren't included at all in this project.

##--------------------------- Faction Classification
//
//  These options matter if the above options are turned on.
//    Example: Tanks = true; hercs = false; Terran = true; Knight = false;
//      Only tanks will appear. Only Terran tanks will be spawned. Knight tanks
//      will not appear at all

//  $allow["Terran"]  : boolean
//    true : Will spawn in Terran vehicles.
$allow["Terran"]    = true;

//  $allow["Knight"]  : boolean
//    true : Will spawn in Knight vehicles.
$allow["Knight"]    = true;

//  $allow["Pirate"]  : boolean
//    true : Will spawn in Pirate vehicles.
$allow["Pirate"]    = true;

//  $allow["Rebel"]  : boolean
//    true : Will spawn in Rebel vehicles.
$allow["Rebel"]     = true;

//  $allow["Cybrid"]  : boolean
//    true : Will spawn in Cybrid vehicles.
$allow["Cybrid"]    = true;

//  $allow["Metagan"] : boolean
//    true : Will spawn in Metagan vehicles.
$allow["Metagan"]   = true;

//  $allow["Special"] : boolean
//    true : Will spawn in Special vehicles.
$allow["Special"]   = false;

##--------------------------- Specialty Classification
//
//  These last two categories are in respects to groups of vehicles that players
//    may find highly annoying, or undesirable for normal game play. They still 
//    require their Faction Classification to be enabled to show up.

//  $allow["Disruptors"] : boolean
//    true : Will spawn in Disruptor Tanks.
$allow["Disruptors"]= false;

//  $allow["Artillery"] : boolean
//    true : Will spawn in Artillery Tanks.
$allow["Artillery"] = false;



##--------------------------- Functions
// DON'T EDIT BELOW THIS LINE.
function getNPC(%name, %hercs, %tanks, %ter, %kni, %pir, %reb, %cyb, %met, %spe, %dis, %art) 
{
    if (%name == "")
        %name = "Swarmie Boi";
    if (%hercs == "")
        %hercs = $allow["Hercs"];
    if (%tanks == "")
        %tanks = $allow["tanks"];
    if (%ter == "")
        %ter = $allow["Terran"];
    if (%kni == "")
        %kni = $allow["Knight"];
    if (%pir == "")
        %pir = $allow["Pirate"];
    if (%reb == "")
        %reg = $allow["Rebel"];
    if (%cyb == "")
        %cyb = $allow["Cybrid"];
    if (%met == "")
        %met = $allow["Metagen"];
    if (%spe == "")
        %spe = $allow["Special"];
    if (%dis == "")
        %dis = $allow["Disruptors"];
    if (%art == "")
        %art = $allow["Artillery"];

    %swarmCount = 0;
    %swarmMembers[-1] = true;

    if ($allow["Hercs"] == true)
    {
        if ($allow["Terran"] == true)
        {
            %swarmMembers[%swarmCount++] = "1"; // Terran Apocalypse
            %swarmMembers[%swarmCount++] = "2"; // Terran Minotaur
            %swarmMembers[%swarmCount++] = "3"; // Terran Gorgon
            %swarmMembers[%swarmCount++] = "4"; // Terran Talon
            %swarmMembers[%swarmCount++] = "5"; // Terran Basilisk
        }
        if ($allow["Knight"] == true)
        {
            %swarmMembers[%swarmCount++] = "10"; // Knight's Apocalypse
            %swarmMembers[%swarmCount++] = "11"; // Knight's Minotaur
            %swarmMembers[%swarmCount++] = "12"; // Knight's Gorgon
            %swarmMembers[%swarmCount++] = "13"; // Knight's Talon
            %swarmMembers[%swarmCount++] = "14"; // Knight's Basilisk
        }
        if ($allow["Cybrid"] == true)
        {
            %swarmMembers[%swarmCount++] = "20"; // Cybrid Seeker
            %swarmMembers[%swarmCount++] = "21"; // Cybrid Goad
            %swarmMembers[%swarmCount++] = "22"; // Cybrid Shepherd
            %swarmMembers[%swarmCount++] = "23"; // Cybrid Adjudicator
            %swarmMembers[%swarmCount++] = "24"; // Cybrid Executioner
            %swarmMembers[%swarmCount++] = "27"; // Platinum Adjudicator (SP version, not selectable)
            %swarmMembers[%swarmCount++] = "28"; // Platinum Executioner (SP version, not selectable)
            %swarmMembers[%swarmCount++] = "55"; // Platinum Adjudicator 2
            %swarmMembers[%swarmCount++] = "56"; // Platinum Executioner 2
        }
        if ($allow["Metagen"] == true)
        {
            %swarmMembers[%swarmCount++] = "35"; // Metagen Seeker
            %swarmMembers[%swarmCount++] = "36"; // Metagen Goad
            %swarmMembers[%swarmCount++] = "37"; // Metagen Shepherd
            %swarmMembers[%swarmCount++] = "38"; // Metagen Adjudicator
            %swarmMembers[%swarmCount++] = "39"; // Metagen Executioner
        }
        if ($allow["Rebel"] == true)
        {
            %swarmMembers[%swarmCount++] = "30"; // Rebel Emancipator
            %swarmMembers[%swarmCount++] = "33"; // Rebel Olympian
        }
        if ($allow["Special"] == true)
        {
            %swarmMembers[%swarmCount++] = "29"; // Prometheus
            %swarmMembers[%swarmCount++] = "40"; // Harabec's Apocalypse
            %swarmMembers[%swarmCount++] = "42"; // Caanan's Basilisk
        }
        if ($allow["Pirate"] == true)
        {
            %swarmMembers[%swarmCount++] = "50"; // Pirate's Apocalypse
            %swarmMembers[%swarmCount++] = "52"; // Pirate's Emancipator
        }
    }

    if ($allow["Tanks"] == true)
    {
        if ($allow["Terran"] == true)
        {
            %swarmMembers[%swarmCount++] = "6"; // Paladin Tank
            %swarmMembers[%swarmCount++] = "7"; // Myrmidon Tank
            if ($allow["Disruptors"] == true)
            {
                %swarmMembers[%swarmCount++] = "8"; // Disruptor Tank
            }
            if ($allow["Artillery"] == true)
            {
                %swarmMembers[%swarmCount++] = "133"; // Nike Artillery
                %swarmMembers[%swarmCount++] = "134"; // Supressor Tank
            }
        }
        if ($allow["Knight"] == true)
        {
            %swarmMembers[%swarmCount++] = "15"; // Knight's Paladin
            %swarmMembers[%swarmCount++] = "16"; // Knight's Myrmidon
            if ($allow["Disruptors"] == true)
            {
                %swarmMembers[%swarmCount++] = "17"; // Knight's Disruptor
            }
        }
        if ($allow["Cybrid"] == true)
        {
            %swarmMembers[%swarmCount++] = "25"; // Bolo Tank
            %swarmMembers[%swarmCount++] = "26"; // Recluse Tank
            if ($allow["Artillery"] == true)
            {
                %swarmMembers[%swarmCount++] = "90"; // Cybrid Artillery
            }
        }
        if ($allow["Rebel"] == true)
        {
            %swarmMembers[%swarmCount++] = "31"; // Avenger Tank
            %swarmMembers[%swarmCount++] = "32"; // Dreadnought Tank
            %swarmMembers[%swarmCount++] = "72";  // Rebel Thumper
            %swarmMembers[%swarmCount++] = "138"; // Rebel bike
            if ($allow["Artillery"] == true)
            {
                %swarmMembers[%swarmCount++] = "137"; // Rebel Artillery
                %swarmMembers[%swarmCount++] = "150"; // SUAV Bus
            }
        }
        if ($allow["Special"] == true)
        {
            %swarmMembers[%swarmCount++] = "41"; // Harabec's Predator
            %swarmMembers[%swarmCount++] = "45"; // Harabec's Super Predator
        }
        if ($allow["Pirate"] == true)
        {
            %swarmMembers[%swarmCount++] = "51"; // Pirate's Dreadlock
        }
    }

    if ($allow["Drone"] == "Not Happening" && true == false)
    {
        if ($allow["Terran"] == true)
        {
            %swarmMembers[%swarmCount++] = "60";  // Terran Empty Cargo
            %swarmMembers[%swarmCount++] = "61";  // Terran Ammo Cargo
            %swarmMembers[%swarmCount++] = "62";  // Terran Big Ammo Cargo
            %swarmMembers[%swarmCount++] = "63";  // Terran Big Personnel Cargo
            %swarmMembers[%swarmCount++] = "64";  // Terran Fuel Cargo
            %swarmMembers[%swarmCount++] = "65";  // Terran Minotaur Cargo
            %swarmMembers[%swarmCount++] = "71";  // Terran Utility Truck
            %swarmMembers[%swarmCount++] = "73";  // Terran Starefield
        }
            if ($allow["Rebel"] == true)
        {
            %swarmMembers[%swarmCount++] = "66";  // Rebel Empty Cargo
            %swarmMembers[%swarmCount++] = "67";  // Rebel Ammo Cargo
            %swarmMembers[%swarmCount++] = "68";  // Rebel Big Cargo Transport
            %swarmMembers[%swarmCount++] = "69";  // Rebel Bix Box Cargo Transport
            %swarmMembers[%swarmCount++] = "70";  // Rebel Box Cargo Transport
        }
        if ($allow["Cybrid"] == true)
        {
            %swarmMembers[%swarmCount++] = "94";  // Cybrid Omnicrawler
            %swarmMembers[%swarmCount++] = "95";  // Cybrid Protector
            %swarmMembers[%swarmCount++] = "96";  // Cybrid Jamma
        }
    }

    %rand = randomInt(1,%swarmCount);
     //  loadobject(%name, "wilzuun\\vehicles\\vehId_" @ %swarmMembers[%rand] @ ".veh");
    %obj = %swarmMembers[%rand];
    return loadobject(%name, "wilzuun\\vehicles\\vehId_" @ %swarmMembers[%rand] @ ".veh");
}