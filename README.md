# Wilzuun Scripts Library
##### By ^TFW^ Wilzuun

## Installing Project
Place `WilzuunLoader.cs` and `WilzuunStdLib.cs` in the Multiplayer directory.
Place the `wilzuun` directory in the Multiplayer directory.

At the end of the mission script file, put `exec("WilzuunLoader.cs");`
Either in the `server_*.cs` file or the mission script file, put `$wilzuun::GameType = "dov";` Or any other game type included in this project.

This README.md file is undergoing Reconstruction currently.

This README.md file is being split into several files, so that each directory has a REDME.md file in it to explain those categories more effectively, while trying to keep this one minimalized. Those files are currently being worked on tonight, Aug 16th, 2020.
