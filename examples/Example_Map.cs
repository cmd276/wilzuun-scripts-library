##---------------------------------------------------------------------- Header
//  Filename: examples\Example_Map.cs
//  AUTHOR: ^TFW^ Wilzuun
##----------------------------------------------------------------------- Notes
//      This file is an example file for how to setup a map using any game type
// from the WIlzuun Library. 

##------------------------------------------------------------- Scoreboard Info

function initScoreBoard()
{
   wilzuun::initScoreBoard();
}

function setRules()
{
    wilzuun::setRules();
}
setRules(); // Call this after the function has been defined.

##------------------------------------------------------- Mission Related Funcs
function onMissionEnd() // Mission::onEnd
{
    wilzuun::onMissionEnd();
}

function onMissionLoad()
{
    wilzuun::onMissionLoad();
}

function onMissionStart()
{
    wilzuun::onMissionStart();
}

##-------------------------------------------------------- Player Related Funcs
function player::onAdd(%player)
{
    wilzuun::player::onAdd(%player);
}

function player::onRemove(%player)
{
    wilzuun::player::onRemove(%player);
}

function setDefaultMissionOptions()
{
    wilzuun::setDefaultMissionOptions();
}

##------------------------------------------------------- Vehicle Related Funcs
##-----------------------------------------------------------------------------
function vehicle::onAdd(%vehicleId)
{
    wilzuun::vehicle::onAdd(%vehicleId);
}

function vehicle::onDestroyed(%destroyed, %destroyer)
{
    wilzuun::vehicle::onDestroyed(%destroyed, %destroyer)
}

function vehicle::onAttacked(%attacked, %attacker)
{
    wilzuun::vehicle::onAttacked(%attacked, %attacker)
}
