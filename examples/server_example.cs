// FILENAME: Server_DOV.cs
exec(serverLocation);

//-------------------------------------------------
// This is what game type we're going to run. Along with what game type settings we want to override.
// 
$wilzuun::GameType = "dov";
function wilzuun::OverRide ()
{
	$swarmClone         =  true; // We want a game where we get Clones of the driver.
    $fixedSwarm         = false; // We want it make the swarm grow at a custom rate.
    $swarmDynanic       =    20; // Every 20 kills we should add one new spawn rate count.
    $pvpReward          = false; // We also don't want PVP rewards. (Players wont get rewards from killing players.)
}
//-------------------------------------------------
//-------------------------------------------------


$server::Hostname =                   "Clone Overrun";
$server::Password =                                "";
$server::MaxPlayers =                              64;
$server::FragLimit =                                0;
$server::TimeLimit =                               10;
$server::MassLimit =                                0;
$server::CombatValueLimit =                         0;
$server::TeamPlay =                           FALSE  ;
$server::TeamMassLimit =                            0;
$server::TeamCombatValueLimit =                     0;
$server::TechLevelLimit =                           0;
$server::DropInProgress =                      TRUE  ;
$server::AllowMixedTech =                      TRUE  ;
$server::FactoryVehOnly =                     FALSE  ;
$server::AllowTeamRed =                        TRUE  ;
$server::AllowTeamYellow =                    FALSE  ;
$server::AllowTeamBlue =                       TRUE  ;
$server::AllowTeamPurple =                    FALSE  ;
//-------------------------------------------------
// This section is custom to my servers. You wont see this is default server configuration files.
for ($i = 0; $i < 1000; $i++) {
    $MissionCycling::Stage[$i] =                            "DM_Avalanche";
    $MissionCycling::Stage[$i++] =                          "DM_Bloody_Brunch";
    $MissionCycling::Stage[$i++] =                          "DM_City_on_the_Edge";
    $MissionCycling::Stage[$i++] =                          "DM_Cold_Titan_Night";
    $MissionCycling::Stage[$i++] =                          "DM_Fear_in_Isolation";
    $MissionCycling::Stage[$i++] =                          "DM_Heavens_Peak";
    $MissionCycling::Stage[$i++] =                          "DM_Impact";
    $MissionCycling::Stage[$i++] =                          "DM_Lunacy";
    $MissionCycling::Stage[$i++] =                          "DM_Mercury_Rising";
    $MissionCycling::Stage[$i++] =                          "DM_Moonstrike";
    $MissionCycling::Stage[$i++] =                          "DM_Requiem_for_Gen_Lanz";
    $MissionCycling::Stage[$i++] =                          "DM_Sacrifice_to_Bast";
    $MissionCycling::Stage[$i++] =                          "DM_State_of_Confusion";
    $MissionCycling::Stage[$i++] =                          "DM_The_Guardian";
    $MissionCycling::Stage[$i++] =                          "DM_Twin_Siege";
    $MissionCycling::Stage[$i++] =                          "DM_Terran_conquest";
}
$Map::Count = $i;
// End custom section.
//-------------------------------------------------

$server::Mission =          $MissionCycling::Stage0;
function setAllowedItems()
{
	//Vehicles
	allowVehicle(           all,  TRUE  );
	//weapons
	allowWeapon(            all,  TRUE  );
	//Components
	allowComponent(         all,  TRUE  );
}

// DOV has a special Override function that allows server admins to override the default settings so that they can run a second server with minimal changes
// while keeping default settings for all other items. The override function can be stored in each map script file as well.
function DOVOverRide ()
{
	$swarmClone         = true;
}
