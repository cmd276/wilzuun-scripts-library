# Wilzuun Scripts Library
##### By ^TFW^ Wilzuun

## Installing, Code Modifications
It is suggested to put the code-block in the `server_*.cs` file. Place anywhere in the file. (I would suggest end of file.)

```javascript
// We want this map to use the DOV game type.
$wilzuun::GameType = "dov";

// DOV has a special Override function that allows server admins to override the default settings so that they can run a second server with minimal changes
// while keeping default settings for all other items. The override function can be stored in each map script file as well.
function DOVOverRide ()
{
	$swarmClone         = true;
}
```

This next block is suggested for any maps that you want to be able to run the library on. (Place at the end of the map script file.)

```javascript
// All lines below this comment are added for making sure the Wilzuun Scripts Library works correctly.
exec("WilzuunLoader.cs");
```